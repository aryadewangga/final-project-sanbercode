<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//Post Controller
Route::get("/", "PostController@index");
Route::post("/post", "PostController@store");
Route::delete("/post/{id}", "PostController@destroy");
Route::get("/post/{id}/edit", "PostController@edit");
Route::put("/post/{id}", "PostController@update");

// search Controller
Route::get('search/', 'SearchController@index');

// Profile Controller
Route::get("{username}", 'ProfileController@index');
Route::get("/{username}/create", "ProfileController@create");
Route::post("/{username}", "ProfileController@store");
Route::get("/{username}/show", "ProfileController@show");
Route::get("/{username}/edit", "ProfileController@edit");
Route::put("/{username}", "ProfileController@update");
// Route::post("{username}/follow", "FollowController@store");

// Follow Controller
Route::post("/follow/{id}", "FollowController@store");
Route::delete("/follow/{id}", "FollowController@destroy");
Route::get("{username}/followers", "FollowController@followers");
Route::get("{username}/following", "FollowController@following");

//Likes Post Controller
Route::post("likes/post/{id}", "LikesPostController@store");
Route::delete("likes/post/{id}", "LikesPostController@destroy");
Route::get("{username}/post/likes/{id}", "LikesPostController@show");

//Likes Comment Controller
Route::post("likes/comment/{id}", "LikesCommentController@store");
Route::delete("likes/comment/{id}", "LikesCommentController@destroy");

//Comment Controller
Route::post("comment/{id}", "CommentController@store");
Route::delete("comment/{id}", "CommentController@destroy");
Route::put("comment/{id}", "CommentController@update");
Route::get("comment/{id}/edit", "CommentController@edit");
