@forelse ($posts as $post)
<div class="card gedf-card mb-3 mt-3">
    <div class="card-header">
        <div class="d-flex justify-content-between align-items-center">
            <a style="text-decoration: none" href="/{{ $post->user->username }}">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="mr-2">
                        @if($post->user->profile != null)
                        <img class="rounded-circle profile-image" width="45" src=" {{asset("uploads/profile_picture/" . $post->user->profile->image)}} " alt="">
                        @else
                        <img class="rounded-circle profile-image" width="45" src=" {{asset("uploads/profile_picture/blank_picture.png")}} " alt="">
                        @endif
                    </div>
                    <div class="ml-2">
                        <div class="h5 m-0">{{ '@'. " " . $post->user->username }}</div>
                    </div>
                </div>
            </a>
            <div>
                @if(Auth::user()->id == $post->user->id)
                <div class="dropdown">
                    <button class="btn btn-link dropdown-toggle" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-ellipsis-h"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop1">
                        <div class="h6 dropdown-header">Configuration</div>
                        <a class="dropdown-item" class="btn btn-primary" data-toggle="modal" data-target="#editModal" data-id="{{ $post->id }}">Edit</a>
                        <a class="dropdown-item" onclick="destroy('{{ $post->id }}')">Delete</a>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="card-body">
        @php
        $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $post->updated_at);

        $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', now());

        $diff = $to->diffInMinutes($from);

        // echo $to;
        if ($diff > 1440) {
        $diff = $to->diffInDays($from) . " " . 'Days';
        } elseif ($diff > 60) {
        $diff = " " . $to->diffInHours($from) . " " . 'Hours';
        }else {
        $diff = " " . $to->diffInMinutes($from) . " " . "Mins";
        }


        @endphp
        <div class="text-muted h7 mb-2"> <i class="fa fa-clock-o"></i>{{ $diff }} ago</div>
        @if ($post->image)
        <img class="img-fluid" src="{{ asset("uploads/post/$post->image") }}" alt="">
        <p class="mt-2 card-text">{{ $post->content }}</p>
        @else
        <p class="mt-2 card-text">{{ $post->content }}</p>
        @endif

    </div>
    <div class="card-footer">
        @if ($post->likes_post->contains('user_id', Auth::user()->id))
        <a onclick="unlike({{ $post->id }})" class="card-link"><i class="fas fa-thumbs-down"></i> {{$post->likes_post->count()}} Unlike</a>
        @else
        <a onclick="like({{ $post->id }})" class="card-link"><i class="fas fa-thumbs-up"></i> {{$post->likes_post->count()}} Like</a>
        @endif
        <a class="card-link"><i class="fa fa-comment"></i> {{$post->comment->count()}} Comment</a>
    </div>

    <!-- comment -->
    @foreach($post->comment as $comment)
    @php
    $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $comment->updated_at);

    $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', now());

    $diff = $to->diffInMinutes($from);

    // echo $to;
    if ($diff > 1440) {
    $diff = $to->diffInDays($from) . " " . 'Days';
    } elseif ($diff > 60) {
    $diff = " " . $to->diffInHours($from) . " " . 'Hours';
    }else {
    $diff = " " . $to->diffInMinutes($from) . " " . "Mins";
    }


    @endphp
    <div class="container bootstrap snippets bootdey">
        <div class="row">
            <div class="col-md-12">
                <div class="blog-comment">
                    <hr />
                    <ul class="comments">
                        <li class="clearfix">
                            @if($comment->user->profile != null)
                            <img class="avatar rounded-circle profile-image" src="{{asset("uploads/profile_picture/" . $comment->user->profile->image)}} " alt="">
                            @else
                            <img class="avatar rounded-circle profile-image" src="{{asset("uploads/profile_picture/blank_picture.png")}} " alt="">
                            @endif
                            <div class="post-comments">
                                <div class="row d-flex justify-content-between align-items-center">
                                    <p class="meta ml-3">{{$comment->user->username}} <small>{{$diff}} ago</small></p>
                                    @if(Auth::user()->id == $comment->user->id)
                                    <div class="dropdown">
                                        <button class="btn btn-link dropdown-toggle" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-h"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop1">
                                            <div class="h6 dropdown-header">Configuration</div>
                                            <a class="dropdown-item" class="btn btn-primary" data-toggle="modal" data-target="#editComment" data-id="{{ $comment->id }}">Edit</a>
                                            <a class="dropdown-item" onclick="destroyComment('{{ $comment->id }}')">Delete</a>
                                        </div>
                                    </div>
                                    @endif
                                </div>

                                <p>
                                    {{$comment->content}}.
                                </p>
                                <div class="align-items-end">
                                    @if ($comment->likes_comment->contains('user_id', Auth::user()->id))
                                    <a onclick="unlike_comment({{ $comment->id }})" class="card-link"><i class="fas fa-thumbs-down"></i> {{$comment->likes_comment->count()}} Unlike</a>
                                    @else
                                    <a onclick="like_comment({{ $comment->id }})" class="card-link"><i class="fas fa-thumbs-up"></i> {{$comment->likes_comment->count()}} Like</a>
                                    @endif
                                </div>
                            </div>

                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>

    @endforeach
    {{-- <form class="m-3" action="/comment/{{$post->id}}" method="post">
    @csrf --}}
    <div class="mr-3 ml-3 mt-2">
        <textarea required class="form-control comment_content" placeholder="Reply"></textarea>
        <input type="hidden" class="post_id_comment" value="{{$post->id}}">
        <button type="button" class="btn btn-primary mt-2 mb-2 submitComment">Reply</button>
    </div>
    {{-- </form> --}}

    <!-- comment end -->

</div>

@empty

@endforelse

{{-- modal --}}
<div class="modal fade" id="editComment" tabindex="-1" role="dialog" aria-labelledby="editCommentLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editCommentLabel">Edit Komentar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{-- <form id="form-edit" method="POST" action="comment/{{ old('id_comment') }}"> --}}
                @csrf
                @method('PUT')
                <div class="form-group">
                    <input name="id_comment" id="id_comment" type="hidden" value="{{ old('id_comment') }}">
                    <div class="form-group mt-2">
                        <label for="message">Komentar</label>
                        <textarea name="edit_comment" id="edit_comment" class="form-control @error('edit_comment') is-invalid @enderror" rows="3" placeholder="Komentar Kamu" required>
                        {{ old('edit_comment') }}
                        </textarea>
                        @error('edit_comment')
                        <div class="invalid-feedback" role="alert">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                {{-- </form> --}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{-- <input form="form-edit" id="submit" type="submit" class="btn btn-primary" value="Edit Komentar"> --}}
                <input id="submitEditComment" type="submit" class="btn btn-primary" value="Edit Komentar">
            </div>
        </div>
    </div>
</div>

@push('js')
<script>
    $('#editComment').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        var modal = $(this)
        $.ajax({
            url: `comment/${id}/edit`,
            type: 'GET',
            dataType: 'JSON',
            success: function(result) {
                // modal.find('form').attr('action', `comment/${id}`);
                modal.find('#id_comment').val(id);
                modal.find('#edit_comment').val(result.content);
            }
        });
    })

    @error('edit_comment')
    $('#editComment').modal('show');
    @enderror

    $("#submitEditComment").click(function(params) {
        let modal = $("#editComment");
        // modal.find('#id_comment');
        let id = modal.find('#id_comment').val();
        let comment = modal.find('#edit_comment').val();
        $.ajax({
            url: `comment/${id}`,
            type: 'PUT',
            dataType: 'JSON',
            data: {
                "comment": comment,
                "_token": token,
            },
            success: function(result) {
                if (result == 'success') {
                    location.reload();
                }
            }
        });
    });

    $(".submitComment").click(function(params) {
        let comment = $(this).closest('div').find('.comment_content').val();
        let post_id = $(this).closest('div').find('.post_id_comment').val();
        $.ajax({
            url: `comment/${post_id}`,
            type: 'POST',
            dataType: 'JSON',
            data: {
                "comment": comment,
                "post_id": post_id,
                "_token": token,
            },
            success: function(result) {
                if (result == 'success') {
                    location.reload();
                }
            }
        });
    });


    function destroyComment(params) {
        let id = params;
        swal({
                title: "Are you sure to delete this comment?",
                text: "Once deleted, you will not be able to recover this comment!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: "DELETE",
                        data: {
                            "id": id,
                            "_token": token,
                        },
                        url: `comment/${id}`,
                        dataType: "JSON",
                        success: function(result) {
                            swal(result.message, {
                                    icon: "success",
                                })
                                .then(() => {
                                    location.reload();
                                });
                        }
                    })
                } else {
                    swal("Your comment file is safe!");
                }
            });
    }


    function like(id) {
        $.ajax({
            type: "post",
            data: {
                "post_id": id,
                "_token": token,
            },
            url: `likes/post/${id}`,
            dataType: "JSON",
            success: function(result) {
                if (result == "success") {
                    location.reload();
                }
            }
        })
    }

    function unlike(id) {
        $.ajax({
            type: "delete",
            data: {
                "post_id": id,
                "_token": token,
            },
            url: `likes/post/${id}`,
            dataType: "JSON",
            success: function(result) {
                if (result == "success") {
                    location.reload();
                }
            }
        })
    }

    function like_comment(id) {
        $.ajax({
            type: "post",
            data: {
                "comment_id": id,
                "_token": token,
            },
            url: `likes/comment/${id}`,
            dataType: "JSON",
            success: function(result) {
                if (result == "success") {
                    location.reload();
                }
            }
        })
    }

    function unlike_comment(id) {
        $.ajax({
            type: "delete",
            data: {
                "comment_id": id,
                "_token": token,
            },
            url: `likes/comment/${id}`,
            dataType: "JSON",
            success: function(result) {
                if (result == "success") {
                    location.reload();
                }
            }
        })
    }
</script>
@endpush