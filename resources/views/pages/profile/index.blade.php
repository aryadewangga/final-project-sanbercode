@extends('layouts.app')

@push("styles")
<link href="{{ asset('css/pages/profile/index.css') }}" rel="stylesheet">
@endpush

@section('content')

@section("title")
Profile {{$user->username}} | Page
@endsection

<div class="container db-social">
    <div class="jumbotron jumbotron-fluid"></div>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-xl-11">
                <div class="widget head-profile has-shadow">
                    <div class="widget-body pb-0">
                        <div class="row d-flex align-items-center">
                            <div class="col-xl-4 col-md-4 d-flex justify-content-lg-start justify-content-md-start justify-content-center">
                                <ul>
                                    <li>
                                        <div class="counter">
                                            {{$user->follow2->count()}}
                                        </div>
                                        <div class="heading">Followers</div>
                                    </li>
                                    <li>
                                        <div class="counter">
                                            {{$user->follow1->count()}}
                                        </div>
                                        <div class="heading">Following</div>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xl-4 col-md-4 d-flex justify-content-center">
                                @if($user->profile != null)
                                <div class="image-default">
                                    <img class="img-thumbnail rounded-circle" src="{{asset("uploads/profile_picture/" . $user->profile->image)}}" alt="tidak muncul">
                                </div>
                                <div class="infos">
                                    <h2>
                                        {{$user->username}}
                                    </h2>
                                    <div class="location">
                                        {{$user->profile->address}}
                                    </div>
                                </div>
                                @else
                                <div class="image-default">
                                    <img class="rounded-circle" src="{{asset("uploads/profile_picture/blank_picture.png")}}" alt="tidak muncul">
                                </div>
                                <div class="infos">
                                    <h2>
                                        {{$user->username}}
                                    </h2>
                                    <div class="location">
                                        No Address
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="col-xl-4 col-md-4 d-flex justify-content-lg-end justify-content-md-end justify-content-center">
                                @if(Auth::user()->username != $user->username)

                                <div class="follow">
                                    @if (!$following)
                                    <form action="/follow/{{$user->id}}" method="post">
                                        @csrf
                                        <input type="hidden" name="user_id_2" value=" {{$user->id}} ">
                                        <button type="submit" class="btn btn-shadow bg-success font-weight-bold text-light" style="width: 150px;">
                                            Follow
                                        </button>
                                    </form>
                                    @else
                                    <form action="/follow/{{ $following->id }}" method="post">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-shadow bg-danger font-weight-bold text-light" style="width: 150px;">
                                            Unfollow
                                        </button>
                                    </form>
                                    @endif
                                </div>

                                @else
                                <a href="/{{$user->username}}/show" class="btn btn-shadow bg-success font-weight-bold text-light">Show Profile</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6 gedf-main">
                @include("layouts.post")
            </div>
        </div>

    </div>
</div>

@endsection