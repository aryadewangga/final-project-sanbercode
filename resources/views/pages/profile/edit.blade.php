@extends('layouts.app')

@push("styles")
<link href="{{ asset('css/pages/profile/edit.css') }}" rel="stylesheet">
@endpush

@section("title")
Edit Profile {{$user->username}} | Page
@endsection

@section('content')
<div class="container edit-profile">
    <div class="row">
        <div class="col-lg-4 pb-5">

            <!-- Account Sidebar-->
            <div class="author-card pb-3">
                <div class="author-card-profile p-3">
                    <div class="author-card-avatar"><img src=" {{asset("uploads/profile_picture/" . $user->profile->image)}}" alt="">
                    </div>
                    <div class="author-card-details">
                        <h5 class="author-card-name text-lg"> {{$user->username}} </h5><span class="author-card-position">Joined {{$user->created_at}} </span>
                    </div>
                </div>
            </div>
            <div class="wizard">
                <nav class="list-group list-group-flush">

                    <a class="list-group-item active" href="#">
                        <i class="fe-icon-user text-muted"></i>Profile Update</a>
                </nav>
            </div>
        </div>


        <!-- Profile Settings-->

        <div class="col-lg-8 pb-5">
            <div class="card p-4">
                <form class="row" action="/{{$user->username}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method("put")
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="account-fn">Fullname</label>
                            <input class="form-control @error('fullname') is-invalid @enderror" type="text" id="account-fn" name="fullname" value="{{ old("fullname", $user->profile->fullname) }}">
                            @error('fullname')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="gender">Gender</label>
                            <select class="form-control @error('gender') is-invalid @enderror" id="gender" name="gender">
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select>
                            @error('gender')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="account-email">E-mail</label>
                            <input class="form-control @error('email') is-invalid @enderror" type="email" id="account-email" name="email" value="{{$user->email}}" disabled>
                            @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="birth_place">Birth Place</label>
                            <input class="form-control @error('birth_place') is-invalid @enderror" type="text" id="birth_place" name="birth_place" value="{{ old("birth_place", $user->profile->birth_place) }}">
                            @error('birth_place')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="birth_date">Birth Date</label>
                            <input class="form-control @error('birth_date') is-invalid @enderror" type="date" id="birth_date" name="birth_date" value="{{ old("birth_date", $user->profile->birth_date) }}">
                            @error('birth_date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="image">Upload Profile Picture</label>
                            <input class="form-control @error('image') is-invalid @enderror" type="file" id="image" name="image" value="{{ old("image", $user->profile->image) }}">
                            @error('image')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                    <div class="form-group">
                        <label for="account-pass">New Password</label>
                        <input class="form-control" type="password" id="account-pass">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="account-confirm-pass">Confirm Password</label>
                        <input class="form-control" type="password" id="account-confirm-pass">
                    </div>
                </div> -->
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="account-confirm-pass">address</label>
                            <textarea class="form-control @error('address') is-invalid @enderror" name="address" id="account-confirm-pass">
                            {{old("address", $user->profile->address)}}
                            </textarea>
                            @error('address')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-12 d-flex justify-content-around">

                        <a role="button" href="/{{$user->username}}/show" class="btn btn-primary">Back</a>
                        <input type="submit" value="Update Profile" class="btn btn-primary" style="width: 150px;">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
