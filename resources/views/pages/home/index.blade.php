@extends('layouts.app')

@push("styles")
<link href="{{ asset('css/pages/home/index.css') }}" rel="stylesheet">
@endpush

@section("title")
Home | Page
@endsection

@section('content')

<div class="container gedf-wrapper mt-5">
    <div class="row">
        <div class="col-md-3 ">
            <div class="card position-fixed card-profile">
                <div class="card-body">
                    <div class="h5">@ {{Auth::user()->username}}</div>
                    @if(Auth::user()->profile != null)
                    <div class="h7 text-muted">{{Auth::user()->profile->fullname}}</div>

                    <div class="h7">{{Auth::user()->profile->address}}
                    </div>
                    @else
                    <div class="h7 text-muted">No Profile</div>
                    @endif
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="h6 text-muted">Followers</div>
                        <div class="h5">{{Auth::user()->follow2->count()}}</div>
                    </li>
                    <li class="list-group-item">
                        <div class="h6 text-muted">Following</div>
                        <div class="h5">{{Auth::user()->follow1->count()}}</div>
                    </li>
                    <li class="list-group-item"></li>
                </ul>
            </div>
        </div>
        <div class="col-md-6 gedf-main">
            <!--- \\\\\\\Post-->
            <div class="card gedf-card">
                <div class="card-header">
                    <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link text-dark @if(!$errors->has('photo') && !$errors->has('caption')) active @endif" id="posts-tab" data-toggle="tab" href="#posts" role="tab" aria-controls="posts" aria-selected="true">Make
                                a publication</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-dark @if($errors->has('photo') || $errors->has('caption')) active @endif" id="images-tab" data-toggle="tab" role="tab" aria-controls="images" aria-selected="false" href="#images">Images</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade @if(!$errors->has('photo') && !$errors->has('caption')) show active @endif" id="posts" role="tabpanel" aria-labelledby="posts-tab">
                            <form action="/post" method="post">
                                @csrf
                                <input name="typePost" type="hidden" value="post">
                                <div class="form-group">
                                    <label class="sr-only" for="message">post</label>
                                    <textarea name="content" class="form-control @error('content') is-invalid @enderror" id="message" rows="3" placeholder="What are you thinking?"></textarea>
                                    @error('content')
                                    <div class="invalid-feedback" role="alert">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-primary">share</button>
                                </div>
                                <!-- <div class="btn-group">
                                    <button id="btnGroupDrop1" type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-globe"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
                                        <a class="dropdown-item" href="#"><i class="fa fa-globe"></i> Public</a>
                                        <a class="dropdown-item" href="#"><i class="fa fa-users"></i> Friends</a>
                                        <a class="dropdown-item" href="#"><i class="fa fa-user"></i> Just me</a>
                                    </div>
                                </div> -->
                            </form>
                        </div>
                        <div class="tab-pane fade @if($errors->has('photo') || $errors->has('caption')) show active @endif" id="images" role="tabpanel" aria-labelledby="images-tab">
                            <form action="/post" method="post" enctype="multipart/form-data">
                                @csrf
                                <input name="typePost" type="hidden" value="post-image">
                                <div class="form-group">
                                    <div class="custom-file">
                                        <input type="file" name="photo" class="custom-file-input @error('photo') is-invalid @enderror" id="validatedCustomFile" required>
                                        <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                        @error('photo')
                                        <div class="invalid-feedback" role="alert">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group mt-2">
                                        <label class="sr-only" for="message">post</label>
                                        <textarea name="caption" class="form-control @error('caption') is-invalid @enderror" rows="3" placeholder="Caption" required></textarea>
                                        @error('caption')
                                        <div class="invalid-feedback" role="alert">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-primary">share</button>
                                </div>
                                <div class="btn-group">
                                    <button id="btnGroupDrop1" type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-globe"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
                                        <a class="dropdown-item" href="#"><i class="fa fa-globe"></i> Public</a>
                                        <a class="dropdown-item" href="#"><i class="fa fa-users"></i> Friends</a>
                                        <a class="dropdown-item" href="#"><i class="fa fa-user"></i> Just me</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Post /////-->
            @include('layouts.post')
            <!--- \\\\\\\Post-->
        </div>
    </div>
</div>

{{-- modal --}}
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Edit Postingan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-edit" method="POST" action="post/{{ old('id_post') }}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <input name="id_post" id="id_post" type="hidden" value="{{ old('id_post') }}">
                        <div class="form-group mt-2">
                            <label for="message">Postingan</label>
                            <textarea name="edit_caption" id="edit_caption" class="form-control @error('edit_caption') is-invalid @enderror" rows="3" placeholder="Postingan Kamu"></textarea>
                            @error('edit_caption')
                            <div class="invalid-feedback" role="alert">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        {{-- <div class="custom-file">
                            <input  type="file" name="edit_photo" class="custom-file-input @error('edit_photo') is-invalid @enderror" id="edit_photo" required>
                            <label class="custom-file-label" for="edit_photo">Choose file...</label>
                            @error('edit_photo')
                            <div class="invalid-feedback" role="alert">
                                {{ $message }}
                    </div>
                    @enderror
            </div> --}}
        </div>
        </form>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input form="form-edit" type="submit" class="btn btn-primary" value="Edit Post">
    </div>
</div>

@endsection

@push('js')
<script>

    @if(session('success'))
    swal("{{ session('success') }}", {
        icon: "success"
    });
    @endif
    $('#editModal').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        var modal = $(this)
        $.ajax({
            url: `post/${id}/edit`,
            type: 'GET',
            dataType: 'JSON',
            success: function(result) {
                // modal.find('#edit_photo').val(recipient)

                modal.find('form').attr('action', `post/${id}`);
                modal.find('#id_post').val(id);
                modal.find('#edit_caption').val(result.content);
            }
        });
    })
    @error('edit_caption')
    $('#editModal').modal('show');
    @enderror

    function destroy(params) {
        let id = params;
        swal({
                title: "Are you sure to delete this Post?",
                text: "Once deleted, you will not be able to recover this Post!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: "DELETE",
                        data: {
                            "id": id,
                            "_token": token,
                        },
                        url: `post/${id}`,
                        dataType: "JSON",
                        success: function(result) {
                            swal(result.message, {
                                    icon: "success",
                                })
                                .then(() => {
                                    location.reload();
                                });
                        }
                    })
                } else {
                    swal("Your Post file is safe!");
                }
            });
    }
</script>
@endpush