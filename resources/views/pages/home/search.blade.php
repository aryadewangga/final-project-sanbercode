@extends('layouts.app')

@push("styles")
<link href="{{ asset('css/pages/home/index.css') }}" rel="stylesheet">
@endpush

@section("title")
Search {{$search}} | Page
@endsection


@section('content')


<div class="container gedf-wrapper">
    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <div class="h5">@ {{Auth::user()->username}}</div>
                    @if(Auth::user()->profile != null)
                    <div class="h7 text-muted">{{Auth::user()->profile->fullname}}</div>

                    <div class="h7">{{Auth::user()->profile->address}}
                    </div>
                    @else
                    <div class="h7 text-muted">No Profile</div>
                    @endif
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="h6 text-muted">Followers</div>
                        <div class="h5">{{Auth::user()->follow2->count()}}</div>
                    </li>
                    <li class="list-group-item">
                        <div class="h6 text-muted">Following</div>
                        <div class="h5">{{Auth::user()->follow1->count()}}</div>
                    </li>
                    <li class="list-group-item"></li>
                </ul>
            </div>
        </div>
        <div class="col-md-9 gedf-main">
            <!-- User /////-->
            @forelse ($users as $user)
            <div class="card gedf-card mb-3">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <a href="/{{$user->username}}" style="text-decoration: none;">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="mr-2">
                                    @if($user->image != null)
                                    <img class="rounded-circle" width="45" id="profile-image" src="{{asset("uploads/profile_picture/" . $user->image)}}" alt="">
                                    @else
                                    <img class="rounded-circle" width="45" src="{{asset("uploads/profile_picture/blank_picture.png")}}" alt="">
                                    @endif
                                </div>
                                <div class="ml-2">
                                    <div class="h5 m-0">{{ $user->username }}</div>
                                    @if($user->fullname != null)
                                    <div class="h7 text-muted">{{$user->fullname}}</div>
                                    @else
                                    <div class="h7 text-muted">No Profile</div>
                                    @endif
                                </div>
                            </div>
                        </a>
                        <div>
                            <div class="dropdown row">
                                @if ($user->id_follow == null)
                                <form action="/follow/{{$user->id}}" class="mr-2" method="post">
                                    @csrf
                                    <input name="user_id_2" type="hidden" value="{{ $user->id }}">
                                    <button type="submit" class="btn btn-shadow bg-success font-weight-bold text-light" style="width: 150px;">
                                        Follow
                                    </button>
                                </form>
                                @else
                                <form action="follow/{{ $user->id_follow }}" class="mr-2" method="post">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-shadow bg-danger font-weight-bold text-light" style="width: 150px;">
                                        Unfollow
                                    </button>
                                </form>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @empty
            <div class="card gedf-card mb-3">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <h5>Tidak Ada User</h5>
                    </div>
                </div>
            </div>
            @endforelse
            <!--- \\\\\\\User-->
        </div>
    </div>
</div>

@endsection