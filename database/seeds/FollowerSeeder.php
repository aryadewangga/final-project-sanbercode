<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FollowerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; $i++) {
            for ($k = 1; $k <= 10; $k++) {
                if ($i != $k) {
                    DB::table('follows')->insert([
                        'user_id_1' => $i,
                        'user_id_2' => $k,
                    ]);
                }
            }
        }
    }
}
