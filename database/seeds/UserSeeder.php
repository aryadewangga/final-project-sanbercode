<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for ($i = 1; $i <= 10; $i++) {

            $name = $faker->firstName;
            DB::table('users')->insert([
                'username' => $name,
                'email' => $name . "@gmail.com",
                'password' => Hash::make('password'),
            ]);
        }
    }
}
