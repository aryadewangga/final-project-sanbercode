<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use App\Follow;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($username)
    {
        $user = User::where("username", $username)->first();
        // check apakah user mengfollow profile
        $following = Follow::where('user_id_1', Auth::user()->id)->where('user_id_2', $user->id)->first();
        //menuju halaman profile beserta daftar postnya
        $posts = $user->post;
        $following = Follow::where('user_id_1', Auth::user()->id)->where('user_id_2', $user->id)->first();
        return view('pages.profile.index', compact("user", "following", "posts"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($username)
    {

        $user = User::where("username", $username)->first();
        //menuju halaman create profiles
        return view("pages.profile.create", compact("user"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            "fullname" => "required",
            "birth_place" => "required",
            "birth_date" => "required",
            "gender" => "required",
            "address" => "required",
            "image" => "mimes:jpeg,jpg,png|max:2200"
        ]);

        if ($request->has("image")) {
            $path = "uploads/profile_picture/";
            $image = $request->image;
            $new_image = time() . " - " . $image->getClientOriginalName();
            $image->move($path, $new_image);
            Profile::create([
                "fullname" => $request->fullname,
                "birth_place" => $request->birth_place,
                "birth_date" => $request->birth_date,
                "gender" => $request->gender,
                "address" => $request->address,
                "image" => $new_image,
                "user_id" => Auth::user()->id
            ]);
        } else {
            Profile::create([
                "fullname" => $request->fullname,
                "birth_place" => $request->birth_place,
                "birth_date" => $request->birth_date,
                "gender" => $request->gender,
                "address" => $request->address,
                "image" => "blank_picture.png",
                "user_id" => Auth::user()->id
            ]);
        }

        //memuju halaman kumpulan post
        return redirect("/");
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {
        $user = User::where("username", $username)->first();

        //menuju halaman tentang detail profile
        return view("pages.profile.show", compact("user"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($username)
    {
        $user = User::where("username", $username)->first();

        //menuju halaman edit profile
        return view('pages.profile.edit', compact("user"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($username, Request $request)
    {
        $user = User::where("username", $username)->first();
        $profile = $user->profile;

        $this->validate($request, [
            "fullname" => "required",
            "birth_place" => "required",
            "birth_date" => "required",
            "gender" => "required",
            "address" => "required",
            "image" => "mimes:jpeg,jpg,png|max:2200"
        ]);

        if ($request->has("image")) {
            $path = "uploads/profile_picture/";

            //jika image nya adalah blank_picture file imagenya jangan dihapus
            if ($profile->image != "blank_picture.png") {
                File::delete($path . $profile->image);
            }

            $image = $request->image;
            $new_image = time() . " - " . $image->getClientOriginalName();
            $image->move($path, $new_image);
            Profile::where("id", $profile->id)->update([
                "fullname" => $request->fullname,
                "birth_place" => $request->birth_place,
                "birth_date" => $request->birth_date,
                "gender" => $request->gender,
                "address" => $request->address,
                "image" => $new_image,
            ]);
        } else {
            Profile::where("id", $profile->id)->update([
                "fullname" => $request->fullname,
                "birth_place" => $request->birth_place,
                "birth_date" => $request->birth_date,
                "gender" => $request->gender,
                "address" => $request->address,
            ]);
        }
        //memuju halaman detail profile
        return redirect("$username/show");
    }
}
