<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Likes_Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($post_id, Request $request)
    {

        $this->validate($request, [
            "comment" => "required"
        ]);

        Comment::create([
            "content" => $request->comment,
            "user_id" => Auth::user()->id,
            "post_id" => $post_id
        ]);

        return response()->json(['success'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comment = Comment::find($id);

        //menuju halaman menampilkan seluruh comment
        return response()->json(['content' => $comment->content], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->has("comment")) {
            Comment::where("id", $id)->update([
                "content" => $request->comment
            ]);
        };
        return response()->json(['success'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Likes_Comment::where('comment_id', $id)->delete();
        Comment::destroy($id);
        //return ke page sebelumnya
        return response()->json(['message' => 'Komentar Berhasil Dihapus'], 200);
    }
}
