<?php

namespace App\Http\Controllers;

use App\Likes_Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikesCommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Likes_Comment::create([
            "user_id" => Auth::user()->id,
            "comment_id" => $request->comment_id
        ]);

        //return ke page sebelumnya
        return response()->json(['success'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Likes_Post  $likes_Post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Likes_Comment::where('comment_id', $id)->where('user_id', Auth::user()->id)->delete();

        //return ke page sebelumnya
        return response()->json(['success'], 200);
    }
}
