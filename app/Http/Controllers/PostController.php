<?php

namespace App\Http\Controllers;

use App\Post;
use App\Follow;
use App\Comment;
use App\Likes_Comment;
use App\Likes_Post;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // ambil id yang di follow
        $followers = Follow::where('user_id_1', Auth::user()->id)->get('user_id_2');
        // jadikan array
        $followers_id = collect($followers->toArray());
        // tambahkan id
        $followers_id->prepend(Auth::user()->id);
        // ambil data post
        $posts = Post::whereIn('user_id', $followers_id->values())->orderBy('updated_at', 'desc')->get();
        $posts->load('user.profile');
        return view('pages.home.index', compact('posts'));
    }


    public function validator(Request $request)
    {
        $rules = ['content' => 'required'];
        if ($request->typePost == 'post-image') {
            $rules = [
                'caption' => 'required',
                'photo' => 'required|mimes:jpeg,bmp,png'
            ];
        }
        $request->validate($rules);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator($request);
        switch ($request->typePost) {
            case 'post':
                Post::create([
                    'content' => $request->content,
                    'user_id' =>  Auth::id()
                ]);
                break;
            case 'post-image':
                $photo = $request->photo;
                $fileName = time() . $photo->getClientOriginalName();
                Post::create([
                    'content' => $request->caption,
                    'user_id' =>  Auth::id(),
                    'image' => $fileName
                ]);
                $photo->move('uploads/post/', $fileName);
                break;
            default:
                echo 'gagal';
                break;
        }
        return redirect("/")->with('success', 'Postingan Berhasil Ditambahkan');;
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        if ($post->image) {
            File::delete('uploads/post/' . $post->image);
        }
        // dd($post->user->id);
        $likes = Likes_Post::where('post_id', $id);
        if ($likes) {
            $likes->delete();
        }
        $comment = Comment::where('user_id', $post->user->id)->first();
        if ($comment) {
            Likes_Comment::where('comment_id', $comment->id)->delete();
        }
        $comment->delete();

        $post->delete();
        return response()->json(['message' => 'Postingan Berhasil Dihapus'], 200);
    }

    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return response()->json(['content' => $post->content], 200);
    }

    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        $request->validate([
            'edit_caption' => 'required'
        ]);
        $post->content = $request->edit_caption;
        $post->save();
        return redirect("/")->with('success', 'Postingan Berhasil Diupdate');
    }
}
