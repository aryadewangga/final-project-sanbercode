<?php

namespace App\Http\Controllers;

use App\Follow;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FollowController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Follow::create([
            "user_id_1" => Auth::user()->id,
            "user_id_2" => $request->user_id_2
        ]);

        // return ke page sebelumnya
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Follow  $follow
     * @return \Illuminate\Http\Response
     */
    public function followers($username)
    {
        $user2 = User::where("username", $username)->first();

        //menuju halaman orang orang yang di follow
        return view("", compact("user2"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Follow  $follow
     * @return \Illuminate\Http\Response
     */
    public function following($username)
    {
        $user1 = User::where("username", $username)->first();

        //menuju halaman orang orang yang di follow
        return view("", compact("user1"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Follow  $follow
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $follow = Follow::destroy($id);
        //return ke page sebelumnya
        return redirect()->back();
    }
}
