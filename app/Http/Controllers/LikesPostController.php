<?php

namespace App\Http\Controllers;

use App\Likes_Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikesPostController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request);
        Likes_Post::create([
            "user_id" => Auth::user()->id,
            "post_id" => $request->post_id
        ]);
        //return ke page sebelumnya
        return response()->json(['success'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Likes_Post  $likes_Post
     * @return \Illuminate\Http\Response
     */
    public function show($username, $id)
    {
        $likes_post = Likes_Post::find($id);

        //menuju ke halaman orang orang yang ngelike postnya
        return view("", compact("likes_post"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Likes_Post  $likes_Post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Likes_Post::where('post_id', $id)->where('user_id', Auth::user()->id)->delete();

        //return ke page sebelumnya
        return response()->json(['success'], 200);
    }
}
