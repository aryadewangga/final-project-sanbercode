<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $data = DB::table('users')
            ->leftJoin('follows', function ($join) {
                $join->on('users.id', '=', 'follows.user_id_2')
                    ->where('follows.user_id_1', '=', Auth::user()->id);
            })->leftJoin("profiles", function ($join) {
                $join->on("users.id", "=", "profiles.user_id");
            })
            ->where('users.username', 'like', "%$request->username%")
            ->where('users.id', '!=', Auth::user()->id)
            ->get(["profiles.*", 'users.*', 'follows.id as id_follow']);
        $data = [
            'users' => $data,
            "search" => $request->username
        ];
        return view('pages.home.search', $data);
    }
}
