<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $guarded = [];
    protected $fillable = [
        'content',
        'user_id',
        'post_id',
    ];

    public function user()
    {
        return $this->belongsTo("App\User");
    }

    public function post()
    {
        return $this->belongsTo("App\Post");
    }

    public function likes_comment()
    {
        return $this->hasMany("App\Likes_Comment");
    }
}
