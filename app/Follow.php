<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    protected $fillable = [
        'user_id_1',
        'user_id_2'
    ];

    public function user()
    {
        //user_id_1 yang ngefollow
        return $this->belongsTo("App\User", "user_id_1", 'id');
    }

    public function user2()
    {
        //user_id_2 yang difollow
        return $this->belongsTo("App\User", "user_id_2", 'id');
    }
}
