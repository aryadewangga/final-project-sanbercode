<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function post()
    {
        return $this->hasMany("App\Post", "user_id", "id");
    }

    public function profile()
    {
        return $this->hasOne("App\Profile");
    }

    public function follow1()
    {
        //user_id_1 yang ngefollow
        return $this->hasMany("App\Follow", "user_id_1");
    }

    public function follow2()
    {
        //user_id_2 yang difollow
        return $this->hasMany("App\Follow", "user_id_2");
    }

    public function comment()
    {
        return $this->hasMany("App\Comment");
    }

    public function likes_post()
    {
        return $this->hasMany("App\Likes_Post");
    }

    public function likes_comment()
    {
        return $this->hasMany("App\Likes_Comment");
    }
}
