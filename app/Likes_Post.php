<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Likes_Post extends Model
{
    protected $table = "likes_post";
    protected $fillable = [
        'user_id',
        'post_id',
    ];

    public function user()
    {
        return $this->belongsTo("App\User", 'user_id', 'id');
    }

    public function post()
    {
        return $this->belongsTo("App\Post");
    }
}
