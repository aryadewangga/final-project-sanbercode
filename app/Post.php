<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'content',
        'image',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function likes_post()
    {
        return $this->hasMany('App\Likes_Post');
    }

    public function comment()
    {
        return $this->hasMany('App\Comment');
    }
}
