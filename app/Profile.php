<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'fullname',
        'birth_place',
        'birth_date',
        'gender',
        'address',
        'image',
        'user_id'
    ];
    public function user()
    {
        return $this->belongsTo("App\User");
    }
}
